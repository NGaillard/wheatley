<?php

namespace Wheatley\Core\Exception;

use Wheatley\Core\Response;

abstract class WheatleyException extends \Exception
{

    private $response;

    public function __construct(Response $response)
    {
        $this->response = $response;
    }

    public function getResponse()
    {
        return $this->response;
    }
}