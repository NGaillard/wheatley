<?php
namespace Wheatley\Core\Exception;


use Wheatley\Core\Response;

class ControllerNotCompleteException extends WheatleyException {

    public function __construct($controller, $action){
        parent::__construct(new Response("Controller ". $controller . " doesn't have method " . $action, "text/html", 500));
    }
} 