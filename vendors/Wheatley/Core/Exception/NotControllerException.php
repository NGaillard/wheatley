<?php
namespace Wheatley\Core\Exception;


use Wheatley\Core\Response;

class NotControllerException extends WheatleyException {

    public function __construct($controller_name){
        parent::__construct(new Response("Class " . $controller_name . " is not a subclass of Controller", "text/html", 500));
    }
} 