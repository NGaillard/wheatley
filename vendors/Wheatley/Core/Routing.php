<?php
namespace Wheatley\Core;

use Symfony\Component\Yaml\Parser;
use Wheatley\Core\Exception\NotControllerException;
use Wheatley\Core\Exception\ControllerNotCompleteException;
use Wheatley\Core\Exception\WheatleyException;

class Routing
{

    /**
     * @return string The path requested by the user
     */
    private static function parse()
    {
        return substr($_SERVER["REQUEST_URI"], strlen($_SERVER["BASE"]));
    }

    /**
     * Analysis and sending the request to the corresponding controller
     * @throws WheatleyException throw when a route is not defined correctly
     */
    public static function dispatch()
    {
        $expected_route = self::parse();

        $parser = new Parser();
        $global_routes = $parser->parse(file_get_contents("config/routing.yml"));

        foreach ($global_routes as $name => $parameters) {
            $local_routes = $parser->parse(file_get_contents("contents" . DIRECTORY_SEPARATOR . $parameters["namespace"]
                . DIRECTORY_SEPARATOR . "config" . DIRECTORY_SEPARATOR . "routing.yml"));
            if ($local_routes != null) {
                foreach ($local_routes as $local_name => $local_parameters) {
                    if (empty($local_parameters['controller']) or empty($local_parameters['action']) or empty
                        ($local_parameters['path'])
                    ) {
                        throw new \Exception("Not complete route : " . $name . "->" . $local_name);
                    } else {
                        $complete_route = $parameters["prefix"] . ($local_parameters["path"] == "/" ? "" : $local_parameters["path"]);
                        $complete_route = str_replace("//", "/", $complete_route);
                        if ($expected_route == $complete_route) {
                            $controller_class = $parameters["namespace"] . "\\Controller\\" . $local_parameters["controller"];
                            $controller = new $controller_class();
                            if (!is_subclass_of($controller, 'Wheatley\Core\Controller')) {
                                throw new NotControllerException($local_parameters["controller"]);
                            }
                            if (!method_exists($controller, $local_parameters["action"])) {
                                throw new ControllerNotCompleteException($local_parameters["controller"], $local_parameters["action"]);
                            }
                            return $controller->$local_parameters["action"]();
                        }
                    }
                }
            }
        }
        return new Response("Page not found.", "text/html", 404);
    }

    /**
     * @param $routing_name String The string that identifies the path in the local routing.yml
     * @return String URL associated with the routing name
     * @throws \Exception throw when routing name not exist
     */
    public static function url($routing_name)
    {
        $parser = new Parser();
        $global_routes = $parser->parse(file_get_contents("config/routing.yml"));

        foreach ($global_routes as $name => $parameters) {
            $local_routes = $parser->parse(file_get_contents("contents" . DIRECTORY_SEPARATOR . $parameters["namespace"]
                . DIRECTORY_SEPARATOR . "config" . DIRECTORY_SEPARATOR . "routing.yml"));
            foreach ($local_routes as $local_name => $local_parameters) {
                if ($routing_name == $local_name) {
                    $url = $_SERVER["HTTP_HOST"] . $_SERVER["BASE"] . str_replace("//", "/", $parameters["prefix"] .
                            $local_parameters["path"]);
                    if (substr($url, -1) == "/") {
                        $url = substr($url, 0, strlen($url) - 1);
                    }
                    $url = "http://" . $url;
                    return $url;
                }
            }
        }
        throw new \Exception("No URL for action $routing_name");
    }
}