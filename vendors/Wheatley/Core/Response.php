<?php

namespace Wheatley\Core;


class Response
{

    private $contents;
    private $mime;
    private $responseCode;

    public function __construct($contents, $mime, $responseCode)
    {
        $this->contents = $contents;
        $this->mime = $mime;
        $this->responseCode = $responseCode;
    }

    public function display()
    {
        http_response_code($this->responseCode);
        header("Content-Type: " . $this->mime);
        echo $this->contents;
    }
}