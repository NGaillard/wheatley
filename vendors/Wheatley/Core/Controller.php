<?php
namespace Wheatley\Core;

class Controller{

    public function render($view_name, $args = array()){
        // Get file
        $view_path = "contents" . DIRECTORY_SEPARATOR;
        $view_path_explode = explode("::", $view_name);
        $i = 0;
        if($view_path_explode[0] === ""){
            $view_path .= "views" . DIRECTORY_SEPARATOR;
        }
        else{
            for(; $i < count($view_path_explode); $i++){
                if(file_exists($view_path . $view_path_explode[$i])){
                    $view_path .= $view_path_explode[$i] . DIRECTORY_SEPARATOR;
                }
                else{
                    $view_path .= "views" . DIRECTORY_SEPARATOR;
                    break;
                }
            }
        }
        for(; $i < count($view_path_explode); $i++){
            $view_path .= $view_path_explode[$i] . DIRECTORY_SEPARATOR;
        }
        $view_path = substr($view_path, 0, strlen($view_path) - 1) . ".php";

        ob_start();
        include $view_path;
        $contents = ob_get_clean();

        return new Response($contents, "text/html", 200);
    }

    public function generateURL($routing_name){
        return Routing::url($routing_name);
    }
}