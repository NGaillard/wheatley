<?php
use Wheatley\Core\Exception\WheatleyException;
use Wheatley\Core\Routing;

function __autoload($class)
{
    $path = str_replace('\\', DIRECTORY_SEPARATOR, $class);
    if(file_exists("vendors" . DIRECTORY_SEPARATOR . $path . ".php")) {
        require_once "vendors" . DIRECTORY_SEPARATOR . $path . ".php";
    }
    else if(file_exists("contents" . DIRECTORY_SEPARATOR . $path . ".php")){
        require_once "contents" . DIRECTORY_SEPARATOR . $path . ".php";
    }
}

try{
    Routing::dispatch()->display();
}
catch(WheatleyException $e) {
    $e->getResponse()->display();
}