<?php
namespace MyWebSite\Front\Controller;

use Wheatley\Core\Controller;
use Wheatley\Core\Response;

class General extends Controller{

    public function homepage(){
        return self::render("MyWebSite::Front::index", array(
            "nom" => "Chell",
            "about" => $this->generateURL("front_about")
        ));
    }

    public function about(){
        return new Response("about", "text/html", 200);
    }
}